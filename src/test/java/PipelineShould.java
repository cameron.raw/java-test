import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PipelineShould {
    @Test
    public void Run_Tests_Successfully(){
        Pipeline pipeline = new Pipeline();
        assertEquals("Test works", pipeline.getTestString());
    }
}
